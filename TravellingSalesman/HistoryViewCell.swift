//
//  HistoryViewCell.swift
//  TravellingSalesman
//
//  Created by MacBookPro on 16/11/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class HistoryViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
