//
//  HistoryViewController.swift
//  TravellingSalesman
//
//  Created by MacBookPro on 16/11/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit
import CoreData

class HistoryViewController: UITableViewController {

    var fetchedResultsController: NSFetchedResultsController<TravelNotes>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeFetchedResultsController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections,
        sections.count > 0 {
            return sections[section].numberOfObjects
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as? HistoryViewCell else {
            fatalError("Wrong cell type dequeued")
        }
        
        guard let object = self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        cell.dateLabel.text = dateFormatter.string(from: object.created)
        cell.selectionStyle = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let barViewControllers = tabBarController?.viewControllers
        if let matrixViewController = barViewControllers![0] as? MatrixViewController {
            
            guard let object = self.fetchedResultsController?.object(at: indexPath) else {
                fatalError("Attempt to configure cell without a managed object")
            }
            
            if let matrix = NSKeyedUnarchiver.unarchiveObject(with: object.matrix) as? Matrix {
                matrixViewController.matrix = matrix
                matrixViewController.path = object.path
                matrixViewController.drawMatrix()
                matrixViewController.updateResultLabels()
                tabBarController?.selectedIndex = 0
            }
        }
        
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let object = self.fetchedResultsController?.object(at: indexPath) else {
                fatalError("Attempt to configure cell without a managed object")
            }
            let context = AppDelegate.viewContext
            context.delete(object)
            
            do {
                try context.save()
            } catch let error as NSError {
                print("Error While Deleting TravelNote: \(error.userInfo)")
            }
        }
    }
}

extension HistoryViewController: NSFetchedResultsControllerDelegate {
    
        func initializeFetchedResultsController() {
            let request = NSFetchRequest<TravelNotes>(entityName: "TravelNotes")
            let createdSort = NSSortDescriptor(key: "created", ascending: true)
            request.sortDescriptors = [createdSort]
    
            let context = AppDelegate.viewContext
            fetchedResultsController = NSFetchedResultsController<TravelNotes>(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            fetchedResultsController.delegate = self
    
            do {
                try fetchedResultsController.performFetch()
            } catch {
                fatalError("Failed to initialize FetchedResultsController: \(error)")
            }
        }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
