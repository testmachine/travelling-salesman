//
//  TravelNotes+CoreDataProperties.swift
//  TravellingSalesman
//
//  Created by MacBookPro on 16/11/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//
//

import Foundation
import CoreData


extension TravelNotes {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TravelNotes> {
        return NSFetchRequest<TravelNotes>(entityName: "TravelNotes")
    }

    @NSManaged public var created: Date
    @NSManaged public var matrix: Data
    @NSManaged public var path: [Int]

}
