//
//  MatrixViewController.swift
//  TravellingSalesman
//
//  Created by MacBookPro on 14/11/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class MatrixViewController: UIViewController {

    var matrix: Matrix!
    var cells: [MatrixFieldView]!
    var path: [Int]!
    
    @IBOutlet weak var nodesSlider: UISlider!
    @IBOutlet weak var citiesCountLabel: UILabel!
    
    @IBOutlet weak var resultLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        citiesCountLabel.text = String(lroundf(nodesSlider.value))
        resultLabel.text = ""
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        matrix = Matrix(itemCount: lroundf(nodesSlider.value))
        let monteCarlo = MonteCarlo(matrix: matrix!)
        path = monteCarlo.process()
        drawMatrix()
        updateResultLabels()
        
        let context = AppDelegate.viewContext
        let travelNote = TravelNotes(context: context)
        travelNote.created = Date()
        travelNote.matrix = NSKeyedArchiver.archivedData(withRootObject: matrix)
        travelNote.path = path
        
        do {
            try context.save()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    
    }
    
    @IBAction func nodeCountChanged(_ sender: UISlider) {
        citiesCountLabel.text = String(lroundf(sender.value))
    }

    func drawMatrix() {
        eraseMatrixFromScreen()
        let fieldCount = matrix.itemCount
        let screenWidth = view.bounds.width
        let fieldSize = screenWidth * 0.0875
        let fieldWidth = Double(fieldSize)
        let fieldHeight = Double(fieldSize)
        
        var tuples: [(Int, Int)] = []
        for i in 1..<path.count {
            tuples.append((path[i - 1], path[i]))
        }
        
        for i in 0..<fieldCount + 1 {
            for j in 0..<fieldCount + 1 {
                let x = Double(j) * fieldWidth + 5.0
                let y = Double(i) * fieldHeight + 20.0
                let rect = CGRect(x: x, y: y, width: fieldWidth, height: fieldHeight)
                let field: MatrixFieldView
                if i == 0 {
                    field = MatrixFieldView(frame: rect, text: "\(j)")
                } else if j == 0 {
                    field = MatrixFieldView(frame: rect, text: "\(i)")
                } else {
                    field = MatrixFieldView(frame: rect, text: "\((matrix[i - 1, j - 1] == Int.max) ? "∞" : String(matrix[i - 1, j - 1]))")
                }
                if i > 0 && j > 0 {
                    if let index = tuples.index(where: { $0.0 == i - 1 && $0.1 == j - 1 }) {
                        let alpha = CGFloat(1.0 - (0.9 / Double(index + 1)))
                        field.changeColor(color: UIColor.red.withAlphaComponent(alpha))
                    }
                } else {
                    field.changeColor(color: UIColor(red:0.91, green:0.91, blue:0.90, alpha:1.0))
                }
                view.addSubview(field)
            }
        }
    }
    
    func eraseMatrixFromScreen() {
        for view in self.view.subviews {
            if view.isKind(of:MatrixFieldView.self) {
                view.removeFromSuperview()
            }
        }
    }
    
    func updateResultLabels() {
        var res = ""
        for i in 0..<path.count - 1 {
            res += "\(path[i] + 1) -> "
        }
        res += "\(path[path.count - 1] + 1)"
        resultLabel.text = res
        
        nodesSlider.value = Float(matrix.itemCount)
        citiesCountLabel.text = String(matrix.itemCount)
    }
}
