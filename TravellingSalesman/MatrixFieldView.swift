//
//  MatrixFieldView.swift
//  TravellingSalesman
//
//  Created by MacBookPro on 14/11/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class MatrixFieldView: UIView {
    
    convenience init(frame: CGRect, text: String) {
        self.init(frame: frame)
        backgroundColor = UIColor.white
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 10.0, height: 10.0))
        
        label.backgroundColor = UIColor(white: 1, alpha: 0)
        label.text = text
        label.sizeToFit()
        
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1
        
        label.frame.origin = CGPoint(x: frame.width / 2 - 10.0, y: frame.height / 2 - 10.0)
        self.addSubview(label)
    }
    
    func changeColor(color: UIColor) {
        self.backgroundColor = color
    }
    
    

}
