//
//  Matrix.swift
//  TravellingSalesman
//
//  Created by MacBookPro on 14/11/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//
import Foundation

class Matrix: NSObject, NSCoding {
    let itemCount: Int
    var grid: [Int]
    init(itemCount: Int) {
        self.itemCount = itemCount
        grid = Array(repeating: 0, count: itemCount * itemCount)
        for row in 0..<itemCount {
            for column in 0..<itemCount {
                let index = (row * itemCount) + column
                if row == column {
                    grid[index] = Int.max
                } else {
                    var randomValue = Int(arc4random_uniform(10) + 1)
                    grid[index] = randomValue;
                    randomValue = Int(arc4random_uniform(10) + 1)
                    grid[(column * itemCount) + row] = randomValue
                }
            }

        }
    }
    init(grid: [Int]) {
        self.grid = grid
        self.itemCount = Int(Double(grid.count).squareRoot())
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(grid, forKey: "grid")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let grid = aDecoder.decodeObject(forKey: "grid") as? [Int] else {
            return nil
        }
        self.init(grid: grid)
    }
    
    func getRow(row: Int) -> [Int] {
        return Array(grid[row * itemCount..<row * itemCount + itemCount])
    }
    func indexIsValid(row: Int, column: Int) -> Bool {
        return row >= 0 && row < itemCount && column >= 0 && column < itemCount
    }
    subscript(row: Int, column: Int) -> Int {
        get {
            assert(indexIsValid(row: row, column: column), "Index out of range")
            return grid[(row * itemCount) + column]
        }
        set {
            assert(indexIsValid(row: row, column: column), "Index out of range")
            grid[(row * itemCount) + column] = newValue
        }
    }
}
