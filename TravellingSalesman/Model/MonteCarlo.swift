//
//  Dijkstra.swift
//  TravellingSalesman
//
//  Created by MacBookPro on 14/11/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import Foundation

class MonteCarlo {
    let matrix: Matrix
    let count: Int
    
    init(matrix: Matrix) {
        self.matrix = matrix
        count = matrix.itemCount
    }
    
    func process() -> [Int] {
        
        var sum: Int, nodes: [Int], prevSum: Int, prevNodes: [Int]
        var delta: Int
        var pCritical: Double, threshold: Double
        var t: Double = 100.0
        let alpha = 0.8
        prevNodes = Array(0..<count)
        prevSum = getTripLength(nodes: prevNodes)
        
        repeat {
            t *= alpha
            nodes = randomSwap(nodes: prevNodes)
            sum = getTripLength(nodes: nodes)
            delta = prevSum - sum
                if delta > 0 {
                    prevNodes = nodes
                    prevSum = sum
                } else {
                    pCritical = 100 * pow(M_E, Double(-delta) / t)
                    threshold = Double(getRandomIntFrom(max: 100) + 1)
                    if threshold > pCritical {
                        prevNodes = nodes
                        prevSum = sum
                    }
                }
        } while t > 0.05
        return prevNodes
    }
    
    func getTripLength(nodes: [Int]) -> Int {
        var sum = 0, x: Int, y:Int
        for i in 1..<count {
            x = nodes[i - 1]
            y = nodes[i]
            sum += matrix[x, y]
        }
        return sum
    }
    
    func randomSwap(nodes: [Int]) -> [Int] {
        let firstIndex = getRandomIntFrom(max: count)
        var secondIndex: Int
        repeat {
            secondIndex = getRandomIntFrom(max: count)
        } while secondIndex == firstIndex
        var changed = nodes
        changed.swapAt(firstIndex, secondIndex)
        return changed
    }
    
    func getRandomIntFrom(max: Int) -> Int {
        return Int(arc4random_uniform(UInt32(max)))
    }
}
